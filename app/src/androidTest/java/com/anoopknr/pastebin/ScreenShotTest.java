package com.anoopknr.pastebin;

import android.content.Context;
import android.os.SystemClock;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.screenshot.Screenshot;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.ClassRule;

import tools.fastlane.screengrab.FalconScreenshotStrategy;
import tools.fastlane.screengrab.Screengrab;
import tools.fastlane.screengrab.locale.LocaleTestRule;


import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;


import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ScreenShotTest {
    @ClassRule
    public static final LocaleTestRule localeTestRule = new LocaleTestRule();

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testFillForm() {
        Screengrab.setDefaultScreenshotStrategy(new FalconScreenshotStrategy(activityRule.getActivity()));
        Screengrab.screenshot("Basic_Home_Screen");

        onView(withId(R.id.poster)).perform(typeText("C \"Hello, World!\" Program"));

        onView(withId(R.id.content)).perform(typeText("#include <stdio.h>\n" +
                "int main()\n" +
                "{\n" +
                "   // printf() displays the string \n" +
                "   printf(\"Hello, World!\");\n" +
                "   return 0;\n" +
                "}"));
        onView(withId(R.id.content)).perform(closeSoftKeyboard());
        onView(withId(R.id.syntax)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("c"))).perform(click());
        onView(withId(R.id.syntax)).check(matches(withSpinnerText(containsString("c"))));

        Screengrab.screenshot("Filled_Home_Screen");
        onView(withId(R.id.pasteButton)).perform(click());
        Screengrab.screenshot("Post_Action");
    }
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.anoopknr.pastebin", appContext.getPackageName());
    }

}
