package com.anoopknr.pastebin;

public class RecentListItem{
    private String poster;
    private String link;
    public RecentListItem(String poster,String link){
        this.poster=poster;
        this.link=link;
    }
    void setposter(String poster){
        this.poster=poster;
    }
    void  setlink(String link){
        this.link=link;
    }
    String getposter(){
        return poster;
    }
    String getlink(){
        return link;
    }
}