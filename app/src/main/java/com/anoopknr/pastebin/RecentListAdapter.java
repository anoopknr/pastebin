package com.anoopknr.pastebin;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RecentListAdapter extends ArrayAdapter<RecentListItem> {

    private Context recentContext;
    private List<RecentListItem> recentList = new ArrayList<>();

    public  RecentListAdapter(@NonNull Context context ,@LayoutRes ArrayList<RecentListItem> list){
        super(context,0,list);
        recentContext = context;
        recentList = list;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(recentContext).inflate(R.layout.recent_list_item,parent,false);

        RecentListItem recentItem = recentList.get(position);


        TextView name = (TextView) listItem.findViewById(R.id.list_poster);
        name.setText(recentItem.getposter());

        TextView release = (TextView) listItem.findViewById(R.id.list_link);
        release.setText(recentItem.getlink());

        return listItem;
    }

}
