package com.anoopknr.pastebin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class MainActivity extends AppCompatActivity {

    private EditText poster, content;
    private Button pasteButton;
    private Spinner syntax;
    private String savedURL = "";
    private String POSTER = "guest";   // Default name given to poster is guest.
    private String SYNTAX = "text";    // Default type is set to plain text.
    private String EXPIRATION = "day"; // By default document expires in one day.
    private String CONTENT = "";
    private String PASTE_BIN_SITE = "https://paste.ubuntu.com/"; // Site which provids the service
    private String APP_DATE_FORMAT = "dd-MM-yyyy";
    private String APP_FILE_NAME = "HISTORY.dat";
    private URL uri;
    private FileOutputStream outputFileStream;
    private OutputStreamWriter writerStream;

    // Adding Menus
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    // Menu Actions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_about:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                View view = LayoutInflater.from(this).inflate(R.layout.about_dialog, null);
                String verName;
                try {
                    verName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                }
                catch (PackageManager.NameNotFoundException e) {
                    verName = null;
                }
                TextView textView = (TextView)view.findViewById(R.id.aboutVersion);
                textView.setText(getString(R.string.version) +" : v"+ verName);

                builder.setTitle(getString(R.string.about) +"  "+getString(R.string.app_name));
                builder.setView(view);
                builder.setNegativeButton(R.string.ok, null);
                builder.create().show();
                return true;
            case R.id.action_recent:
                startActivity(new Intent(MainActivity.this,RecentActivity.class));
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Adding custom Toolbar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(myToolbar);


        pasteButton = (Button) findViewById(R.id.pasteButton);
        poster = (EditText) findViewById(R.id.poster);
        content = (EditText) findViewById(R.id.content);
        syntax = (Spinner) findViewById(R.id.syntax);

        // Checking App file
        File file = new File(getApplicationContext().getFilesDir(),APP_FILE_NAME);
        try {
            if(!file.exists()){

                outputFileStream = openFileOutput(APP_FILE_NAME,MODE_PRIVATE);
                Snackbar.make(findViewById(R.id.pasteButton), R.string.file_404_error, Snackbar.LENGTH_SHORT).show();
            }
            else {
                outputFileStream = openFileOutput(APP_FILE_NAME,MODE_APPEND);
            }
            writerStream = new OutputStreamWriter(outputFileStream);

        }catch (Exception e){
            e.printStackTrace();
        }



        ArrayAdapter<CharSequence> syntaxAdapter = ArrayAdapter.createFromResource(this, R.array.syntax_array, R.layout.simple_spinner_item);
        syntaxAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        syntax.setAdapter(syntaxAdapter);

        pasteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    POSTER = poster.getText().toString().trim();
                    CONTENT = content.getText().toString().trim();
                    SYNTAX = syntax.getSelectedItem().toString().trim();

                    if (POSTER.isEmpty() || CONTENT.isEmpty()) {
                        Snackbar.make(findViewById(R.id.pasteButton), R.string.null_input_error, Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                    uri = new URL(PASTE_BIN_SITE);
                    savedURL = new GetResponse().execute(uri).get();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (savedURL.isEmpty()) {
                    Snackbar.make(findViewById(R.id.pasteButton), R.string.content_post_error, Snackbar.LENGTH_SHORT).show();
                    return;
                }

                // Saving to file
                try {
                    SimpleDateFormat dateFormater = new SimpleDateFormat(APP_DATE_FORMAT);
                    writerStream.write(dateFormater.format(new Date())+"\n");
                    writerStream.write(poster.getText()+"\n");
                    writerStream.write(savedURL+"\n");

                    writerStream.flush();
                    outputFileStream.flush();

                }catch (Exception e){
                    e.printStackTrace();
                }
                // Alert Box
                AlertDialog.Builder pastebinAlert = new AlertDialog.Builder(MainActivity.this);
                pastebinAlert.setTitle("Saved To Pastebin").setMessage(savedURL).setPositiveButton(R.string.share, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Ubuntu Pastebox");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, savedURL );
                        startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    }
                }).setNegativeButton(R.string.open, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setData(Uri.parse(savedURL));
                        startActivity(intent);
                    }
                });
                AlertDialog dialog = pastebinAlert.create();
                dialog.show();
                content.setText("");
                poster.setText("");
            }
        });

    }

    // function to encode body of POST method.
    public String getEncodedStrings() {
        String ENCODED_STRING = null;
        try {
            POSTER = URLEncoder.encode(POSTER, "UTF-8");
            SYNTAX = URLEncoder.encode(SYNTAX, "UTF-8");
            EXPIRATION = URLEncoder.encode(EXPIRATION, "UTF-8");
            CONTENT = URLEncoder.encode(CONTENT, "UTF-8");

            ENCODED_STRING = "poster=" + POSTER + "&syntax=" + SYNTAX + "&expiration=" + EXPIRATION + "&content=" + CONTENT;

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return ENCODED_STRING;
    }

    // AsyncTask to send code to pastebin server.
    private class GetResponse extends AsyncTask<URL, Integer, String> {

        @Override
        protected String doInBackground(URL... urls) {
            String savedURL = "";
            try {
                HttpsURLConnection connection = (HttpsURLConnection) urls[0].openConnection();

                SSLContext sc;
                sc = SSLContext.getInstance("TLS");
                sc.init(null, null, new SecureRandom());
                connection.setSSLSocketFactory(sc.getSocketFactory());

                connection.setReadTimeout(7000);      // Timeouts set to 7s.
                connection.setConnectTimeout(7000);
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setDoInput(true);


                String StringToPost = getEncodedStrings();

                if (StringToPost != null) {
                    OutputStream outputStream = connection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
                    writer.write(StringToPost);
                    Log.i("PostBody", StringToPost);
                    writer.flush();
                    writer.close();
                    outputStream.close();
                }

                // Connection works only with this line.
                InputStream postInput=connection.getInputStream();

                savedURL = connection.getURL().toString();
                Log.i("New URL", savedURL);

                // Disconnecting
                postInput.close();
                connection.disconnect();

                if (savedURL.equals(PASTE_BIN_SITE)) // Site URL changes is POST was successful.
                {
                    savedURL = "";
                }
                Log.i("New URL After Checking", savedURL);

            } catch (Exception e) {
                if (e instanceof UnknownHostException) {
                    Snackbar.make(findViewById(R.id.pasteButton), R.string.host_error, Snackbar.LENGTH_SHORT).show();
                }
                e.printStackTrace();
            }
            return savedURL;
        }
    }
}
