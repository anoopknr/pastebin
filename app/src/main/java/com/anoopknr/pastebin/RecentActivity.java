package com.anoopknr.pastebin;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class RecentActivity extends AppCompatActivity {
    private String APP_DATE_FORMAT = "dd-MM-yyyy";
    private String APP_FILE_NAME = "HISTORY.dat";
    private ListView recentListView;
    private RecentListAdapter recentlistAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent);

        // Adding custom Toolbar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.recent_post_toolbar);
        setSupportActionBar(myToolbar);
        
        recentListView = (ListView) findViewById(R.id.recent_list);
        ArrayList<RecentListItem> recentList = new ArrayList<>();

        try {
            SimpleDateFormat dateFormater = new SimpleDateFormat(APP_DATE_FORMAT);
            Date currentDate = dateFormater.parse(dateFormater.format(new Date()));
            System.out.println("Cal : " + dateFormater.format(currentDate));

            String tempDateString = "";
            String tempPosterString = "";
            String tempLinkString = "";

            // ArrayLists for storing file contents
            List<String> dateArrayList = new ArrayList<String>();
            List<String> posterArrayList = new ArrayList<String>();
            List<String> linkArrayList = new ArrayList<String>();

            Date savedDate;

            FileInputStream inputFileStream = openFileInput(APP_FILE_NAME);
            BufferedReader readBuffer = new BufferedReader(new InputStreamReader(inputFileStream));

            while ((tempDateString = readBuffer.readLine()) != null) {
                tempPosterString = readBuffer.readLine();
                tempLinkString = readBuffer.readLine();
                savedDate = dateFormater.parse(tempDateString);

                // Addding to ArrayList if links are not out-dated.
                if (!savedDate.before(currentDate)) {
                    dateArrayList.add(tempDateString);
                    posterArrayList.add(tempPosterString);
                    linkArrayList.add(tempLinkString);
                }
            }
            readBuffer.close();
            inputFileStream.close();

            if (dateArrayList.isEmpty()) {
                Snackbar.make(findViewById(R.id.recent_list), R.string.null_posts_error, Snackbar.LENGTH_INDEFINITE).show();
            }
            else {
                // Creating Iterators for ArrayLists
                Iterator dateIterator = dateArrayList.iterator();
                Iterator posterIterator = posterArrayList.iterator();
                Iterator linkIterator = linkArrayList.iterator();

                // Re-writing new file .
                FileOutputStream outputFileStream = openFileOutput(APP_FILE_NAME, MODE_PRIVATE);
                OutputStreamWriter writerStream = new OutputStreamWriter(outputFileStream);
                while (dateIterator.hasNext()) {

                    tempDateString = dateIterator.next().toString();
                    tempPosterString = posterIterator.next().toString();
                    tempLinkString = linkIterator.next().toString();

                    // Adding to list
                    recentList.add(new RecentListItem(tempPosterString, tempLinkString));

                    // writing back to file .
                    writerStream.write(tempDateString + "\n");
                    writerStream.write(tempPosterString + "\n");
                    writerStream.write(tempLinkString + "\n");
                }
                writerStream.flush();
                writerStream.close();
                outputFileStream.flush();
                outputFileStream.close();
            }

            } catch(Exception e){
                if (e instanceof ParseException) {
                    System.out.println("Error in date");

                }
                System.out.println(e);
            }



        recentlistAdapter = new RecentListAdapter(this,recentList);
        recentListView.setAdapter(recentlistAdapter);
    }
}
