# PasteBin
Pastbin is an android base client application for Ubuntu pastebin service , an OpenSource web application where you can paste snippets of text, usually source code or log files, for public viewing. Pastes made to pastebin are usually only saved for a short period of time.

# Screenshots

 <img src="/phoneScreenshots/01.png" alt="screenshot1" height="50%" width="30%"> 
 <img src="/phoneScreenshots/02.png" alt="screenshot2" height="50%" width="30%"> 
 <img src="/phoneScreenshots/03.png" alt="screenshot3" height="50%" width="30%"> 
 <img src="/phoneScreenshots/04.png" alt="screenshot4" height="50%" width="30%"> 
 <img src="/phoneScreenshots/05.png" alt="screenshot5" height="50%" width="30%"> 
 <img src="/phoneScreenshots/06.png" alt="screenshot6" height="50%" width="30%"> 
 
# Installation
<a href="https://f-droid.org/en/packages/com.anoopknr.pastebin/">
    <img src="https://f-droid.org/badge/get-it-on.png"
    alt="Get it on F-Droid" height="80">
</a>

<br>

# License Information

<img src="https://gnu.org/graphics/gplv3-127x51.png" />

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) 
